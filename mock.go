package mock

import "fmt"

type Mock struct {
	Name string
	Link string
}

func HelloMock() {
	fmt.Println("hello mock")
}
